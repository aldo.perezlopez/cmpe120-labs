

Dear Tim Cook,



I hope this letter finds you well, as a long time supporter of apple products
I wanted to bring your attention to something that has been brought to light recently.
This letter is regarding the adoption of International Electrotechnical Commission (IEC)
units in computing and storage specifications. The use of SI units for storage, such as
kilobytes, megabytes, and gigabytes, has led to some confusion, as these units can refer
to either powers of 10 or powers of 2, depending on the context. IEC units like kibibytes (KiB), mebibytes (MiB),
and gibibytes (GiB) eliminate this ambiguity by explicitly denoting powers of 2 (1024, 1024^2, and so forth).
Another reason is consumers often feel misled when they find that the actual usable storage on a device
is less than what the SI unit implies. I for one have made this mistake when cloning an SSD to an
external SSD that uses SI units in powers of 10 rather than powers of 2, resulting in a failure.
I hope this is enough to convince you, toward a brighter vision of clarity and transparency.


Best,

Aldo Perez
