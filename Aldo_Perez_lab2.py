# HW: Python - ASCII

# Write a function to return the uppercase version of a letter
def uppercase(letter):
    # get the ascii
    letter_ascii = ord(letter)
    # check if it is a lowercase letter
    if letter_ascii >= 97 and letter_ascii <= 122:
        # make it uppercase by adding 32
        letter_ascii -= 32
        # return the letter
        return chr(letter_ascii)


# Test
print(uppercase('a'))


# Write a function to return the lowercase version of a letter
def lowercase(letter):
    # get the ascii
    letter_ascii = ord(letter)
    # check if it is a letter
    if letter_ascii >= 65 and letter_ascii <= 90:
        # make it lowercase by subtracting 32
        letter_ascii += 32
        # return the letter
        return chr(letter_ascii)


# Test
print(lowercase('A'))


# Write a function that returns true if the letter is an alphabet

def is_alphabet(letter):
    # get the ascii
    letter_ascii = ord(letter)
    # check if it is a letter
    if (letter_ascii >= 65 and letter_ascii <= 90) or (letter_ascii >= 97 and letter_ascii <= 122):
        return True


# Write a function that returns true if an element is a digit
def is_digit(element):
    # get the ascii
    element_ascii = ord(element)
    # check if it is a digit
    if element_ascii >= 48 and element_ascii <= 57:
        return True


# Test
print(is_digit('1'))


# Write a function to determine if the given character is a special character or not
def is_special_character(element):
    # get the ascii
    element_ascii = ord(element)
    # check if it is a special character
    if (element_ascii >= 32 and element_ascii <= 47) or (element_ascii >= 58 and element_ascii <= 64) or (
            element_ascii >= 91 and element_ascii <= 96) or (element_ascii >= 123 and element_ascii <= 126):
        return True


# Test
print(is_special_character('!'))
