# Create a Python program that takes a number as input from the user and converts it into kilobytes (KB), kibibytes (
# kiB), megabytes (MB), and mebibytes (MiB).

def UnitConverter(num):
    kb = num / 1000
    kib = num / 1024
    mb = num / 1000000
    mib = num / 1048576
    print("Conversions:")
    print(f"{num:,.0f} bytes")
    print(f"{kb:,.3f} kilobytes")
    print(f"{kib:,.3f} kibibytes")
    print(f"{mb:,.6f} megabytes")
    print(f"{mib:,.6f} mebibytes")

print("---------------------------------------------------------------------------------")
print("Enter a number to convert to kilobytes, kibibytes, megabytes, and mebibytes.")
while True:
    print("Enter 'q' to quit.")
    num = input("Enter a number (total bytes): ")
    if num == 'q':
        break
    elif num.isdigit():
        num = int(num)
        UnitConverter(num)
    elif '-' in str(num):
        print("Please enter a positive number.")
    else:
        print("Please enter a number.")
